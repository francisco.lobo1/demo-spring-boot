package com.sngular.learn.example.entity;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Document
public class Alumno {

	@Id
	private String id;
	
	private String nombre;

	private String primerApellido;

	private String segundoApellido;

	private String dni;
	
	private Modulo modulo;
}
