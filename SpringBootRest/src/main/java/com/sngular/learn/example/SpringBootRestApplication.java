package com.sngular.learn.example;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import com.sngular.learn.example.entity.Alumno;
import com.sngular.learn.example.request.AlumnoRequest;
import com.sngular.learn.example.response.AlumnoResponse;

import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.impl.DefaultMapperFactory;

@SpringBootApplication
public class SpringBootRestApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootRestApplication.class, args);
	}

	@Bean
	public MapperFacade alumnoRequestMapperBean() {

		MapperFactory mapperFactory = new DefaultMapperFactory.Builder().build();
		mapperFactory.classMap(AlumnoRequest.class, Alumno.class);
		return mapperFactory.getMapperFacade();
	}

	@Bean
	public MapperFacade alumnoResponseMapperBean() {

		MapperFactory mapperFactory = new DefaultMapperFactory.Builder().build();
		mapperFactory.classMap(AlumnoResponse.class, Alumno.class);
		return mapperFactory.getMapperFacade();
	}

}