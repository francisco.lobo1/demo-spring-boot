package com.sngular.learn.example.mapper;

import java.util.List;

import com.sngular.learn.example.entity.Alumno;
import com.sngular.learn.example.request.AlumnoRequest;
import com.sngular.learn.example.response.AlumnoResponse;

public interface AlumnoMapper {

	public Alumno mapAlumnoReqToAlumno(AlumnoRequest alumnoRequest);

	public AlumnoResponse mapAlumnoToAlumnoRes(Alumno alumno);

	public List<AlumnoResponse> mapearAlumnos(List<Alumno> alumnosList);
}
