package com.sngular.learn.example.response;

import com.sngular.learn.example.request.AlumnoRequest;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class AlumnoResponse extends AlumnoRequest {

	private String id;

}
