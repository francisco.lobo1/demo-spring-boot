package com.sngular.learn.example.request;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class AlumnoRequest {

	@NotEmpty(message = "El campo nombre es obligatorio")
	private String nombre;

	@NotEmpty(message = "El campo primer apellido es obligatorio")
	private String primerApellido;

	@NotEmpty(message = "El campo segundo apellido es obligatorio")
	private String segundoApellido;

	@NotEmpty(message = "El campo dni es obligatorio")
	private String dni;

	@Valid
	@NotNull(message = "El campo modulo no puede ser nulo")
	private ModuloRequest modulo;

}
