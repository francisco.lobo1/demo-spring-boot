package com.sngular.learn.example.request;

import java.util.Date;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.PastOrPresent;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ModuloRequest {

	@NotEmpty(message = "El campo centro es obligatorio")
	private String centro;
	
	private String nombre;

	@PastOrPresent
	private Date fechaInscripcion;
}
