package com.sngular.learn.example.repository.custom;

import java.util.List;

import com.sngular.learn.example.entity.Alumno;

public interface AlumnoRepositoryCustom {

	public Alumno guardarAlumno(Alumno alumno);

	public Alumno obtenerAlumnoDni(String dni);

	public List<Alumno> obtenerAlumnos();

	public Alumno obtenerAlumnoId(String id);

	public void eliminarAlumnoId(String id);

}
