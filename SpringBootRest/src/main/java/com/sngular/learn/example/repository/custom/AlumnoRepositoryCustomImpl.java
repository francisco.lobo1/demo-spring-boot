package com.sngular.learn.example.repository.custom;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.sngular.learn.example.entity.Alumno;
import com.sngular.learn.example.repository.AlumnoRepository;

@Repository
public class AlumnoRepositoryCustomImpl implements AlumnoRepositoryCustom {

	@Autowired
	private AlumnoRepository alumnoRepository;

	@Override
	public Alumno guardarAlumno(Alumno alumno) {
		return alumnoRepository.save(alumno);
	}

	@Override
	public Alumno obtenerAlumnoDni(String dni) {
		return alumnoRepository.findByDni(dni);
	}

	@Override
	public List<Alumno> obtenerAlumnos() {
		return alumnoRepository.findAll();
	}

	@Override
	public Alumno obtenerAlumnoId(String id) {
		Optional<Alumno> alumno = alumnoRepository.findById(id);
		return (alumno.isPresent() == true) ? alumno.get() : null;
	}

	@Override
	public void eliminarAlumnoId(String id) {
		alumnoRepository.deleteById(id);
	}

}
