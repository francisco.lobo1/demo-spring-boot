package com.sngular.learn.example.mapper;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sngular.learn.example.entity.Alumno;
import com.sngular.learn.example.request.AlumnoRequest;
import com.sngular.learn.example.response.AlumnoResponse;

import ma.glasnost.orika.MapperFacade;

@Service
public class AlumnoMapperImpl implements AlumnoMapper {

	private static Logger log = LogManager.getLogger(AlumnoMapperImpl.class);

	@Autowired
	private MapperFacade alumnoRequestMapperBean;

	@Autowired
	private MapperFacade alumnoResponseMapperBean;

	@Override
	public Alumno mapAlumnoReqToAlumno(AlumnoRequest alumnoRequest) {

		log.info("Mapeando AlumnoRequest --> Alumno");
		return alumnoRequestMapperBean.map(alumnoRequest, Alumno.class);
	}

	@Override
	public AlumnoResponse mapAlumnoToAlumnoRes(Alumno alumno) {
		log.info("Mapeando Alumno --> AlumnoDto");
		return alumnoResponseMapperBean.map(alumno, AlumnoResponse.class);
	}

	@Override
	public List<AlumnoResponse> mapearAlumnos(List<Alumno> alumnosList) {

		List<AlumnoResponse> alumnoResponsesList = new ArrayList<>();

		for (Alumno alumno : alumnosList) {
			alumnoResponsesList.add(alumnoResponseMapperBean.map(alumno, AlumnoResponse.class));
		}
		return alumnoResponsesList;
	}

}
