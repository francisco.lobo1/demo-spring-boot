package com.sngular.learn.example.controller;

import java.util.List;

import javax.validation.Valid;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.sngular.learn.example.request.AlumnoRequest;
import com.sngular.learn.example.response.AlumnoResponse;
import com.sngular.learn.example.service.AlumnoService;

@RestController
@RequestMapping(value = "/alumno")
public class AlumnoController {

	private static Logger log = LogManager.getLogger(AlumnoController.class);

	@Autowired
	private AlumnoService alumnoService;

	@RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public AlumnoResponse guardarAlumno(@Valid @RequestBody AlumnoRequest alumnoRequest) {
		log.info("Guardando nuevo {}", alumnoRequest);
		return alumnoService.guardarAlumno(alumnoRequest);
	}

	@RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public List<AlumnoResponse> obtenerAlumnos(@RequestParam(value = "dni", required = false) String dni) {
		if (dni == null) {
			log.info("Obteniendo todos los alumnos");
			return alumnoService.obtenerAlumnos();
		} else {
			log.info("Obteniendo alumnos con dni {}", dni);
			return alumnoService.obtenerAlumnoDni(dni);
		}

	}

	@RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public AlumnoResponse obtenerAlumnoId(@PathVariable(value = "id") String id) {
		log.info("Obteniendo alumno con id: {}", id);
		return alumnoService.obtenerAlumnoId(id);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public void eliminarAlumnoId(@PathVariable(value = "id") String id) {
		log.info("Eliminando alumno con id: {}", id);
		alumnoService.eliminarAlumnoId(id);
	}

}
