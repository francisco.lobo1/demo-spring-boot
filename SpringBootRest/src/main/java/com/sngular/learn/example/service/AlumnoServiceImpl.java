package com.sngular.learn.example.service;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import com.sngular.learn.example.entity.Alumno;
import com.sngular.learn.example.mapper.AlumnoMapper;
import com.sngular.learn.example.repository.custom.AlumnoRepositoryCustom;
import com.sngular.learn.example.request.AlumnoRequest;
import com.sngular.learn.example.response.AlumnoResponse;

@Service
public class AlumnoServiceImpl implements AlumnoService {

	private static Logger log = LogManager.getLogger(AlumnoServiceImpl.class);

	@Autowired
	private AlumnoRepositoryCustom alumnoRepositoryCustom;

	@Autowired
	private AlumnoMapper alumnoMapper;

	@Override
	public AlumnoResponse guardarAlumno(AlumnoRequest alumnoRequest) {

		validarDni(alumnoRequest.getDni());

		if (alumnoRepositoryCustom.obtenerAlumnoDni(alumnoRequest.getDni()) != null) {
			log.info("El alumno con dni {} ya está registrado", alumnoRequest.getDni());
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
					"El alumno con dni " + alumnoRequest.getDni() + " ya está registrado");
		}

		Alumno alumno = alumnoMapper.mapAlumnoReqToAlumno(alumnoRequest);
		alumno = alumnoRepositoryCustom.guardarAlumno(alumno);
		AlumnoResponse alumnoResponse = alumnoMapper.mapAlumnoToAlumnoRes(alumno);
		log.info("Nuevo alumno {} guardado correctamente", alumnoResponse);

		return alumnoResponse;
	}

	@Override
	public List<AlumnoResponse> obtenerAlumnos() {

		List<Alumno> alumnosList = alumnoRepositoryCustom.obtenerAlumnos();

		if (alumnosList.isEmpty()) {
			log.info("No hay alumnos registrados");
			throw new ResponseStatusException(HttpStatus.NO_CONTENT, "No hay alumnos registrados");
		}

		return alumnoMapper.mapearAlumnos(alumnosList);
	}

	@Override
	public List<AlumnoResponse> obtenerAlumnoDni(String dni) {
		validarDni(dni);

		Alumno alumno = alumnoRepositoryCustom.obtenerAlumnoDni(dni);

		if (alumno == null) {
			log.info("El alumno con dni {} no existe", dni);
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, "El alumno con dni " + dni + " no existe");
		}

		List<AlumnoResponse> alumnoResponseList = new ArrayList<>();
		alumnoResponseList.add(alumnoMapper.mapAlumnoToAlumnoRes(alumno));

		return alumnoResponseList;
	}

	@Override
	public void eliminarAlumnoId(String id) {

		Alumno alumno = alumnoRepositoryCustom.obtenerAlumnoId(id);

		if (alumno == null) {
			log.info("El alumno con id {} no existe", id);
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, "El alumno con id " + id + " no existe");
		}

		alumnoRepositoryCustom.eliminarAlumnoId(id);
	}

	@Override
	public AlumnoResponse obtenerAlumnoId(String id) {

		Alumno alumno = alumnoRepositoryCustom.obtenerAlumnoId(id);

		if (alumno == null) {
			log.info("El alumno con id {} no existe", id);
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, "El alumno con id " + id + " no existe");
		}

		return alumnoMapper.mapAlumnoToAlumnoRes(alumno);
	}

	private void validarDni(String dni) {

		Pattern pattern = Pattern.compile("(\\d{1,8})([TRWAGMYFPDXBNJZSQVHLCKEtrwagmyfpdxbnjzsqvhlcke])");
		Matcher matcher = pattern.matcher(dni);

		if (!matcher.matches()) {
			log.info("El DNI introducido no es válido {}", dni);
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "El dni " + dni + " no es válido");
		}
	}
}
