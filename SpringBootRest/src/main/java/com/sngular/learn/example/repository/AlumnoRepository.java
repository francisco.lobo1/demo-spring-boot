package com.sngular.learn.example.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.sngular.learn.example.entity.Alumno;

public interface AlumnoRepository extends MongoRepository<Alumno, String>{

	Alumno findByDni(String dni);
	
}