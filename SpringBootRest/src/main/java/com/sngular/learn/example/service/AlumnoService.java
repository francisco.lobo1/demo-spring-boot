package com.sngular.learn.example.service;

import java.util.List;

import com.sngular.learn.example.request.AlumnoRequest;
import com.sngular.learn.example.response.AlumnoResponse;

public interface AlumnoService {

	AlumnoResponse guardarAlumno(AlumnoRequest alumnoRequest);

	List<AlumnoResponse> obtenerAlumnos();

	List<AlumnoResponse> obtenerAlumnoDni(String dni);

	void eliminarAlumnoId(String id);

	AlumnoResponse obtenerAlumnoId(String id);

}
